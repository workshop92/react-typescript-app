import { createAsyncThunk, createSlice } from '@reduxjs/toolkit'
import type { PayloadAction } from '@reduxjs/toolkit'
import { RootState } from '../store'
import { login } from '../../services/auth.service'
import { LoginFormInput } from './../../types/login-form-input.type';
import { LoginErrorResponse, LoginResponse } from './../../types/login.type';
import { AxiosError } from 'axios';


// Define a type for the slice state
interface AuthState {
  profile: ProfileType
  email: string
  loginResponse: LoginResponse | null
}

interface ProfileType {
  name: string
  permission: string,
  
}

// Define the initial state using that type
const initialState: AuthState = {
    profile: {name: "chaiwichit", permission: "admin"},
    email: "chaiwichit@gmail.com",
    loginResponse: null
}

export const loginThunk = createAsyncThunk<
  LoginResponse | null, 
  LoginFormInput,
  {
    rejectValue: LoginErrorResponse
  }
  >('auth/loginThunkStatus', async (user: LoginFormInput, {rejectWithValue}) => {
  // เรียน api ในนี้ function
  try {
    const response = await login(user.email, user.password)
    // console.log(response.data)
    localStorage.setItem('token', JSON.stringify(response.data))
    return response.data
  } catch (error: any) {
    let err: AxiosError<LoginErrorResponse> = error
    if (!err.response) {
      throw error
    }
    return rejectWithValue(err.response.data)
  }
})




export const authSlice = createSlice({
  name: 'auth',
  // `createSlice` will infer the state type from the `initialState` argument
  initialState: initialState,
  reducers: {
    // กำหนด actions
    updateProfileAction: (state) => {
      state.profile.name="Toom"
      state.profile.permission="user"
      state.email = "Toom@gmail.com"
    }
  },
  extraReducers: (builder) => {
    builder.addCase(loginThunk.fulfilled, (state, action: PayloadAction<LoginResponse | null>) => {
      //action คือสิ่งที่ response
      state.loginResponse = action.payload
    })
  }
})

export const { updateProfileAction } = authSlice.actions
export const selectAuthState = (state: RootState) => state.authState

export default authSlice.reducer