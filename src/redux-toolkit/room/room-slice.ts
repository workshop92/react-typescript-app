import { createAsyncThunk, createSlice } from '@reduxjs/toolkit'
import type { PayloadAction } from '@reduxjs/toolkit'
import { RootState } from '../store'
import { getRoomBooking } from '../../services/room.service'
import { AxiosError } from 'axios';
import { RoomBooking } from './../../types/room+booking.type';


// Define a type for the slice state
interface RoomBookingState {
    roomBooking :RoomBooking | null
}



// Define the initial state using that type
const initialState: RoomBookingState = { 
    roomBooking: null
}


export const getRoomBookingThunk = createAsyncThunk<RoomBooking, void>('auth/getRoomBookingThunkStatus', async () => {
  // เรียน api ในนี้ function
  try {
    const response = await getRoomBooking()
    return response.data
  } catch (error: any) {
    let err: AxiosError<any> = error
    if (!err.response) {
      throw error
    }
    return err.response.data
  }
})




export const roomBookingSlice = createSlice({
  name: 'roomBooking',
  // `createSlice` will infer the state type from the `initialState` argument
  initialState: initialState,
  reducers: {
   
  },
  extraReducers: (builder) => {
    builder.addCase(getRoomBookingThunk.fulfilled, (state, action: PayloadAction<RoomBooking | null>) => {
      //action คือสิ่งที่ response
      state.roomBooking = action.payload
    })
  }
})

// export const { } = roomBookingSlice.actions
export const selectRoomBookingState = (state: RootState) => state.roomBookingStatue

export default roomBookingSlice.reducer