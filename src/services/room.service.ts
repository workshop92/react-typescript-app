import { AxiosResponse, http } from "./http.service";
import { RoomBooking } from './../types/room+booking.type';



export async function getRoomBooking(): Promise<AxiosResponse<RoomBooking, any>> {
    const url = "https://api.codingthailand.com/api/events"
    return await http.get<RoomBooking>(url)
}

