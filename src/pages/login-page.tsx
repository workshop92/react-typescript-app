import {
    Flex,
    Box,
    FormControl,
    FormLabel,
    Input,
    Checkbox,
    Stack,
    Link,
    Button,
    Heading,
    Text,
    useColorModeValue,
    FormErrorMessage,
    useToast,
  } from '@chakra-ui/react';
import { useEffect } from 'react';
import { yupResolver } from '@hookform/resolvers/yup';
import * as yup from "yup";
import { SubmitHandler, useForm } from 'react-hook-form';
import { LoginFormInput } from '../types/login-form-input.type';
import { useAppDispatch } from '../redux-toolkit/hooks';
import { loginThunk } from './../redux-toolkit/auth/auth-slice';
import { LoginErrorResponse } from '../types/login.type';
// import { LoginFormInput } from './../types/login-form-input.type';
import { useNavigate } from 'react-router-dom';



  
export default function LoginPage() {
  const toast = useToast()
  const dispatch = useAppDispatch()
  const navigate = useNavigate()

  const schema = yup.object().shape({
    email: yup.string().required('ระบุอีเมลล์ด้วย').email('รูปแบบ email ไม่ถูกต้อง'),
    password: yup.string().required('ป้อนรหัสผ่านด้วย').min(3,'ขั้นต่ำ 3 ตัวอักษร')
  });


  const { register, handleSubmit, formState: { errors, isLoading, isSubmitting } } = useForm<LoginFormInput>({
    resolver: yupResolver(schema),
    mode: 'all'
  });
  
  const onSubmit: SubmitHandler<LoginFormInput> = async (data: LoginFormInput) => {
    // console.log(data)
    try {
      const result = await dispatch(loginThunk(data)).unwrap()
      // console.log(result?.access_token)

      navigate('/dashboard')
      

    } catch (error: any) {
      let err: LoginErrorResponse = error
      
      toast({
        title: 'Login ไม่สำเร็จ.',
        description: err.message,
        status: 'error',
        duration: 3000,
        isClosable: true,
        position: 'top',
      })

    }
    

    
  }

  // console.log(watch("email")) // watch input value by passing the name of it

  useEffect(() => {
      document.title = 'Login Page';
  }, []);


  return (
    <Flex
      minH={'100vh'}
      align={'center'}
      justify={'center'}
      bg={useColorModeValue('gray.50', 'gray.800')}>
      <form onSubmit={handleSubmit(onSubmit)} noValidate>
        <Stack spacing={8} mx={'auto'} maxW={'lg'} py={12} px={6}>
          <Stack align={'center'}>
            <Heading fontSize={'4xl'}>Sign in to your account</Heading>
          </Stack>
          <Box
            rounded={'lg'}
            bg={useColorModeValue('white', 'gray.700')}
            boxShadow={'lg'}
            p={8}>
            <Stack spacing={4}>
              <FormControl id="email" isInvalid={errors.email ? true : false}>
                <FormLabel>Email address</FormLabel>
                <Input type="email" {...register("email")}/>
                <FormErrorMessage>
                {
                    errors && errors.email?.message
                }
                </FormErrorMessage>
              </FormControl>

              <FormControl id="password" isInvalid={errors.password ? true : false}>
                <FormLabel>Password</FormLabel>
                <Input type="password" {...register("password")}/>
                <FormErrorMessage>
                {
                    errors && errors.password?.message
                }
                </FormErrorMessage>
              </FormControl>

              <Stack spacing={10}>
                <Button
                  isLoading={isSubmitting}
                  loadingText="กำลังโหลด........."
                  type='submit'
                  bg={'blue.400'}
                  color={'white'}
                  _hover={{
                    bg: 'blue.500',
                  }}>
                  Sign in
                </Button>
              </Stack>
              
            </Stack>
          </Box>
        </Stack>
      </form>
    </Flex>
  );
}