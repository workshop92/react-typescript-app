import { useEffect } from "react";
import { Link } from "react-router-dom";
import { selectAuthState, updateProfileAction } from "../redux-toolkit/auth/auth-slice";
import { useAppDispatch, useAppSelector } from "../redux-toolkit/hooks";
import { authSlice } from './../redux-toolkit/auth/auth-slice';
import {
    Text,
    Button,
    Icon,
    IconProps,
    useColorMode,
  } from '@chakra-ui/react';

function AboutPage() {
    const { profile, email } = useAppSelector(selectAuthState)
    const dispatch = useAppDispatch();

    const updateProfile = () => {
      dispatch(updateProfileAction())
    }

    useEffect(() => {
        document.title = 'About Page';
      }, []);
    return ( 
        <>
            <h1>About Page</h1>
            <Text>ยินดีต้อนรับ { profile.name }</Text>
            <Text>Email { email }</Text>
            <Button onClick={updateProfile}>Update Profile</Button>
            <Link to={"/"} replace={true}>
                กลับหน้าหลัก
            </Link>
        </>
     );
}

export default AboutPage;