import { redirect, RouteObject } from "react-router-dom";
import Dhome from "../pages/dashboard/d-home";
import Dlayout from "../pages/dashboard/d-layout";
import DRoom from "../pages/dashboard/d-room";
import { getProfile } from "../services/auth.service";


const routeDashboard: RouteObject[] = [
    {
        path: "/dashboard",
        loader: async () => {
            const response = await getProfile()
            if (!response?.data.data.user) {
                throw redirect('/login');
            }

            return response.data.data.user
        },
        element: <Dlayout />,
        children: [
            {
                path: "", //localhost:4000/dashboard
                element: <Dhome />
            },
            {
                path: "room", //localhost:4000/room
                element: <DRoom />
            }
        ]
    },
]

export default routeDashboard;