import { createBrowserRouter, redirect } from "react-router-dom";
import Dhome from "../pages/dashboard/d-home";
import HomePage from "../pages/home-page";
import LoginPage from "../pages/login-page";
import { getProfile } from "../services/auth.service";
import AboutPage from './../pages/about-page';
import routeDashboard from "./dashboard";

const router = createBrowserRouter([
    {
        path: "/",
        loader: async () => {
            const response = await getProfile()
            if (!response?.data.data.user) {
                throw redirect('/login');
            }

            return response.data.data.user
        },
        element: <HomePage />,
        children: [
        ]
    },
    {
        path: "/about",
        element: <AboutPage />,
    },
    {
        path: "/login",
        element: <LoginPage />,
    },
    ...routeDashboard,

]);


export default router