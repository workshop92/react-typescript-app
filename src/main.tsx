import { Provider } from 'react-redux'
import { store } from '../src/redux-toolkit/store'
import { RouterProvider } from "react-router-dom";
import { ChakraProvider } from '@chakra-ui/react'
import ReactDOM from 'react-dom/client'

import './global.css'

import router from "./routes/root";


ReactDOM.createRoot(document.getElementById('root') as HTMLElement).render(
  // <React.StrictMode>
  <Provider store={store}>
    <ChakraProvider>
      <RouterProvider router={router} />
    </ChakraProvider>
  </Provider>
  // </React.StrictMode>,
)
