import { useEffect, useState } from "react";

type AppHeaderProps = {
    title: string,
    year?: number
}

function AppHeader({title, year}: AppHeaderProps) {
    const [isShow, setIsShow] = useState(false)

    function getHell(){
        console.log('useeffect AppHeader')
    }

    useEffect(() => {
        console.log('useeffect AppHeader')
    },)

    useEffect(() => {
        console.log('useeffect AppHeader 2')
    },[])

    const mouseOver = () => {
        setIsShow(!isShow)
    }

    return ( 
        <>
            <h1 onMouseOver={mouseOver} >{title} {year}</h1>
            {
                isShow && <p>Hello App Header</p>
            }
            
        </>
    );
}

export default AppHeader;