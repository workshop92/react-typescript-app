การเตรียมตัวเรียนหลักสูตรนี้ มีดังนี้

1. ติดตั้ง Node.js เวอร์ชัน LTS (ตอนนี้เวอร์ชัน 18)
https://nodejs.org/dist/v18.12.1/node-v18.12.1-x64.msi

2. ติดตั้งโปรแกรม Visual Studio Code
https://code.visualstudio.com/

3. ติดตั้ง Git
https://git-scm.com/download/win

4. ติดตั้ง Redux DevTools ที่ Chrome, Edge หรือ Firefox ดูลิงก์ที่นี่ครับ
https://github.com/reduxjs/redux-devtools


ลิงก์เข้าเรียนโปรแกรม Zoom

Topic: Build Web App with React TypeScript and Redux Toolkit

Join Zoom Meeting
https://us06web.zoom.us/j/82745834756?pwd=NmdaZmVXQ2VudGMvUmFMQTBDU1I3dz09

Meeting ID: 827 4583 4756
Passcode: 960930


ไว้พบกันครับ วันที่ 19 ธ.ค. 2565 เริ่มเวลา 19.30 น. เป็นต้นไป ครับ

อ.เอก

ป.ล. กรณีดูเป็นวิดีโอย้อนหลังจะมีเมล์ส่งให้หลังเรียนจบครับ หรือเมล์มาขอระหว่างเรียนได้ครับ

รายละเอียดหลักสูตรนี้ https://codingthailand.com/react_typescript_live/


Slide, Resources และวิดีโอย้อนหลัง

https://bit.ly/3uYX4so


โค้ดระหว่างเรียน
https://gitlab.com/codingthailand/react-typescript-course

cd react-typescript-course-main
npm run dev


 Prettier - Code formatter