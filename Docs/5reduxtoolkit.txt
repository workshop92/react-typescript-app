install 
    npm install @reduxjs/toolkit
    (Redux Toolkit includes the Redux core)

    npm install react-redux
    npm install --save-dev @redux-devtools/core



1. สร้าง folder >> src/redux-toolkit

2.สร้าง file store.ts
    import { configureStore } from '@reduxjs/toolkit'
    // ...

    export const store = configureStore({
    reducer: {
    
    },
    })

    // Infer the `RootState` and `AppDispatch` types from the store itself
    export type RootState = ReturnType<typeof store.getState>
    // Inferred type: {posts: PostsState, comments: CommentsState, users: UsersState}
    export type AppDispatch = typeof store.dispatch

3 create file hooks.ts
    import { useDispatch, useSelector } from 'react-redux'
    import type { TypedUseSelectorHook } from 'react-redux'
    import type { RootState, AppDispatch } from './store'

    // Use throughout your app instead of plain `useDispatch` and `useSelector`
    export const useAppDispatch: () => AppDispatch = useDispatch
    export const useAppSelector: TypedUseSelectorHook<RootState> = useSelector


4 setup main.ts

    import React from 'react'
    import ReactDOM from 'react-dom'
    import './index.css'
    import App from './App'
    import { store } from './app/store'
    import { Provider } from 'react-redux'

    ReactDOM.render(
    <Provider store={store}>
        <App />
    </Provider>,
    document.getElementById('root')
    )


5 สร้าง slice เพื่อกำหนด State ตัวอย่าง auth-slice.ts


6 setup ที่ store.ts
    import authReducer from './auth/auth-slice'

    export const store = configureStore({
    reducer: {
    auth: authReducer
    },
    })


การเรียกใช้งาน

    useAppSelector(selectAuthState)



---------------

thunk

type
export type AppThunk<ReturnType = void> = ThunkAction<
  ReturnType,
  RootState,
  unknown,
  AnyAction
>
    ใส่ไว้ใน hook.ts


สร้าง function ที่ silce
export const loginThunk = createAsyncThunk('auth/loginThunkStatus', async (user: LoginFormInput) => {
  // เรียน api ในนี้ function
  try {
    const response = await login(user.email, user.password)
    return response.data
  } catch (error: any) {
    console.log(error)
  }
})


ประกาศตัวแปร state มารับ


เรียกใช้งาน
const dispatch = useAppDispatch()